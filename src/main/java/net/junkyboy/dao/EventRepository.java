package net.junkyboy.dao;

import net.junkyboy.model.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by bgear on 11/20/16.
 *
 * Simple JPA based repository for saving data. Can be targeted to any JDBC source including mysql.
 */
@Repository
public interface EventRepository extends JpaRepository<Event, Long> {

  Optional<List<Event>> findAllByDeviceId(@Param("deviceId") Long deviceId);

  Optional<Event> findByStatus(String status);

}