package net.junkyboy.model;

import lombok.*;

import javax.persistence.Embeddable;

/**
 * Created by bgear on 11/20/16.
 */
@Embeddable
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Reward {

  @NonNull
  private String type;

  @NonNull
  private Long value;

  @Builder
  Reward(String type, Long value) {
    this.type = type;
    this.value = value;
  }
  Reward() {}
}
