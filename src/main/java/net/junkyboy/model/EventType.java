package net.junkyboy.model;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by bgear on 11/20/16.
 */

public enum EventType {

  SLOW_FLO ("slow_flo"),
  COLLECT_FLO ("collect_flo"),
  COOKIE_MADNESS ("cookie_madness"),
  FAST_FLO ("fast_flo");

  private final String type;

  EventType(String type) {
    this.type = type;
  }

  @JsonValue
  public String getType() {
    return type;
  }
}
