package net.junkyboy.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by bgear on 11/20/16.
 */
@Embeddable
@Getter
@Setter
@ToString
@EqualsAndHashCode
@Entity
@JsonPropertyOrder({ "id", "event_type", "goal", "start", "end", "status", "reward" })
public class Event {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @JsonIgnore
  private Long deviceId;

  @JsonFormat(shape = JsonFormat.Shape.STRING)
  @JsonProperty("event_type")
  private EventType eventType;

  private Integer goal;

  @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSX")
  private Date start;

  @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss.SSSX")
  private Date end;

  @JsonProperty("status")
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private Status status;

  @Embedded
  private Reward reward;

  @Builder
  Event(Long id,
        Long deviceId,
        EventType eventType,
        Integer goal,
        Date start,
        Date end,
        Status status,
        Reward reward ) {

    this.id = id;
    this.deviceId = deviceId;
    this.eventType = eventType;
    this.goal = goal;
    this.start = start;
    this.end = end;
    this.status = status;
    this.reward = reward;
  }

  Event() {}
}
