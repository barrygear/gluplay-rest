package net.junkyboy.model;

import com.fasterxml.jackson.annotation.JsonValue;
import net.junkyboy.rest.InvalidRequestException;

/**
 * Created by bgear on 11/20/16.
 */

public enum Status {

  AVAILABLE ("available"),
  ACTIVE ("active"),
  COMPLETED ("completed"),
  COLLECTION ("collection"),
  COLLECTED ("collected");

  private final String status;

  Status(String status) {
    this.status = status;
  }

  @JsonValue
  public String getStatus() {
    return status;
  }

  public static Status statusOf(String value) throws InvalidRequestException {
    for(Status status : Status.values()) {
      if (value.equalsIgnoreCase(status.getStatus())) {
        return status;
      }
    }
    throw new InvalidRequestException("Cant find a match for " + value);
  }
}
