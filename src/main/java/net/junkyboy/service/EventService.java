package net.junkyboy.service;

import lombok.extern.slf4j.Slf4j;
import net.junkyboy.dao.EventRepository;
import net.junkyboy.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * Created by bgear on 11/20/16.
 *
 * EventService class. This is the service layer for accessing events from a datastore. It allows for the
 * abstraction of the back end store. For example, if you wanted to change the backend from jdbc to dynamo
 * or redis, the service layer will stay the same. In this code example, the repository is not dynamically
 * settable but simply changing it to a, for example, redisTemplate to store to redis can be done here.
 */
@Service
@Slf4j
public class EventService {

  @Autowired
  EventRepository eventRepository;

  @PostConstruct
  private void init() {
  }

  public Optional<List<Event>> getEvents(Long deviceId) {
    return eventRepository.findAllByDeviceId(deviceId);
  }

  public Event getEvent(Long eventId) {
    return eventRepository.findOne(eventId);
  }

  public boolean delete(Long eventId) {
    boolean result = true;
    try {
      eventRepository.delete(eventId);
    } catch (Exception error) {
      result = false;
      log.error("Error deleting event_id{}", eventId, error);
    }
    return result;
  }

  public Event save(Event event) {
    boolean result = true;
    try {
      event = eventRepository.save(event);
    } catch (Exception error) {
      log.error("Error saving event{}", event, error);
      event = null;
    }
    return event;
  }

}
