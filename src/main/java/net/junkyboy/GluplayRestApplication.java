package net.junkyboy;

import net.junkyboy.dao.EventRepository;
import net.junkyboy.model.Event;
import net.junkyboy.model.EventType;
import net.junkyboy.model.Reward;
import net.junkyboy.model.Status;
import org.slf4j.event.EventRecodingLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * The main entrypoint of the microservice. This contains the main() and code to populate with default
 * Events. The default events could have been loaded from a config file as well.
 */
@SpringBootApplication
public class GluplayRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(new Object[] {GluplayRestApplication.class,GluplayRestConfiguration.class}, args);
	}

	@Autowired
	List<Event> defaultEvents;

	@Bean
	CommandLineRunner init(EventRepository eventRepository) {
		return (evt) -> defaultEvents.forEach(eventRepository::save);
	}
}
