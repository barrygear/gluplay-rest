package net.junkyboy.rest;

import lombok.extern.slf4j.Slf4j;
import net.junkyboy.model.Event;
import net.junkyboy.model.Status;
import net.junkyboy.service.EventService;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.*;

/**
 * Created by bgear on 11/20/16.
 */
@Slf4j
@RestController
@RequestMapping("/events")
public class EventsRestController {

  @Autowired
  EventService eventService;

  @Autowired
  BeanUtilsBean beanUtilsBean;
  /**
   * events is a REST endpoint that displays the stored events for a device_id
   * @param deviceId
   * @return
   * @throws NotFoundException
     */
  @RequestMapping(value = "", method = RequestMethod.GET)
  public Map<String, List<Event>> events(@RequestParam(value = "device_id") Long deviceId) throws NotFoundException {

    Map<String, List<Event>> results = new HashMap<>();
    List<Event> values =  new ArrayList<>();

    Optional<List<Event>> optionalEvents = eventService.getEvents(deviceId);

    if(optionalEvents.isPresent()) {
      values = optionalEvents.get();
      //throw new NotFoundException("Device not found");
    }
    results.put("events", values);
    return results;
  }

  /**
   * progress REST endoing allows the editing of an existing event_id.
   * @param event_id
   * @param input
   * @return
   * @throws NotFoundException
   * @throws InvalidRequestException
     */
  @RequestMapping(value = "/{event_id}/progress", method = RequestMethod.PUT)
  public Map<String, List<Event>> progress(@PathVariable(value = "event_id") Long event_id, @RequestBody Event input) throws NotFoundException, InvalidRequestException {

    Map<String, List<Event>> results = new HashMap<>();
    List<Event> values =  new ArrayList<>();

    Event event = eventService.getEvent(event_id);
    if(event == null) {
      throw new NotFoundException("event_id " + event_id + " not found");
    }

    if(input.getDeviceId() != null || input.getId() != null) {
      throw new InvalidRequestException("Cannot change id or device_id");
    }

    try {
      beanUtilsBean.copyProperties(event, input);
    } catch (IllegalAccessException e) {
      throw new InvalidRequestException("Error copying bean", e);
    } catch (InvocationTargetException e) {
      throw new InvalidRequestException("Error copying bean", e);
    }

    eventService.save(event);

    values.add(event);
    results.put("events", values);
    return results;
  }

  /**
   * collect REST endpoint allows for the 'consumption' of rewards from completed events
   * @param event_id
   * @return
   * @throws NotFoundException
   * @throws InvalidRequestException
     */
  @RequestMapping(value = "/{event_id}/collect", method = RequestMethod.POST, produces = "application/json")
  public Map<String, List<Event>> collect(@PathVariable(value = "event_id") Long event_id) throws NotFoundException,InvalidRequestException {

    Map<String, List<Event>> results = new HashMap<>();
    List<Event> values =  new ArrayList<>();

    Event event = eventService.getEvent(event_id);
    if(event == null) {
      throw new NotFoundException("event_id " + event_id + " not found");
    } else if(event.getStatus() == Status.COLLECTION) {
      event.setStatus(Status.COLLECTED);
      eventService.save(event);
      log.info("Reward Player with : {}", event.getReward().toString());
      values.add(event);
      results.put("events", values);
      return results;
    } else if(event.getStatus() == Status.COLLECTED) {
      throw new InvalidRequestException("event_id " + event_id + " already collected!");
    } else {
      throw new InvalidRequestException("event_id " + event_id + " not available for collections");
    }
  }
}
