package net.junkyboy.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by bgear on 11/20/16.
 */

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class NotFoundException extends Exception {

  NotFoundException(String message, Throwable cause) {
    super(message, cause);
  }
  NotFoundException(String message) {
    super(message);
  }
}
