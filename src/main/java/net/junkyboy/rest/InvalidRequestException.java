package net.junkyboy.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by bgear on 11/20/16.
 */

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class InvalidRequestException extends Exception {

  InvalidRequestException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidRequestException(String message) {
    super(message);
  }
}
