package net.junkyboy;

import lombok.extern.slf4j.Slf4j;
import net.junkyboy.model.Event;
import net.junkyboy.model.EventType;
import net.junkyboy.model.Reward;
import net.junkyboy.model.Status;
import net.junkyboy.utils.NullAwareBeanUtilsBean;
import org.apache.commons.beanutils.*;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by bgear on 11/20/16.
 */
@EnableAutoConfiguration
@Configuration
@Slf4j
@ComponentScan(basePackageClasses = GluplayRestConfiguration.class)
public class GluplayRestConfiguration {

  class StatusConverter implements Converter {
    @Override
    public <T> T convert(Class<T> tClass, Object o) {
      try {
        return tClass.cast(tClass.getMethod("statusOf", String.class).invoke(null, o.toString()));
      } catch (IllegalAccessException e) {
        log.error("", e);
      } catch (InvocationTargetException e) {
        log.error("", e);
      } catch (NoSuchMethodException e) {
        log.error("", e);
      }
      return null;
    }
  }

  @PostConstruct
  private void init() {
  }

  @Bean
  public BeanUtilsBean beanUtilsBean() {
    BeanUtilsBean beanUtilsBean = new NullAwareBeanUtilsBean();
    beanUtilsBean.getConvertUtils().register(new StatusConverter(), Status.class);
    beanUtilsBean.getConvertUtils().register(new StatusConverter(), Event.class);
    return beanUtilsBean;
  }

  @Bean
  public List<Event> defaultEvents() {
    List<Event> events = new ArrayList<>();

    events.add(Event.builder()
            .deviceId(1L)
            .eventType(EventType.COLLECT_FLO)
            .goal(100)
            .start(new Date())
            .end(new Date())
            .status(Status.COMPLETED)
            .reward(Reward.builder().type("soft_currency").value(10000L).build())
            .build()
    );

    events.add(Event.builder()
            .deviceId(1L)
            .eventType(EventType.COOKIE_MADNESS)
            .goal(125)
            .start(new Date())
            .end(new Date())
            .status(Status.COLLECTION)
            .reward(Reward.builder().type("soft_currency").value(5000L).build())
            .build()
    );

    events.add(Event.builder()
            .deviceId(1L)
            .eventType(EventType.FAST_FLO)
            .goal(80)
            .start(new Date())
            .end(new Date())
            .status(Status.ACTIVE)
            .reward(Reward.builder().type("hard_currency").value(10L).build())
            .build()
    );
    return events;
  }
}
