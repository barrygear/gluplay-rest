package net.junkyboy;

import lombok.extern.slf4j.Slf4j;
import net.junkyboy.dao.EventRepository;
import net.junkyboy.model.Event;
import net.junkyboy.model.EventType;
import net.junkyboy.model.Reward;
import net.junkyboy.model.Status;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.*;

@RunWith(SpringRunner.class)
@Slf4j
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = {GluplayRestConfiguration.class})
//@WebAppConfiguration
public class GluplayRestApplicationTests {

    @Autowired
    List<Event> defaultEvents;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));

    private MockMvc mockMvc;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

//		this.eventRepository.deleteAllInBatch();
//		defaultEvents.forEach(eventRepository::save);
    }

    @Test
    public void deviceNotFound() throws Exception {
        mockMvc.perform(get("/events")
                .param("device_id", "-1")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.events", hasSize(0)));
    }

    @Test
    public void deviceFound() throws Exception {
        mockMvc.perform(get("/events")
                .param("device_id", "1")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.events.*", hasSize(3)));
    }

    @Test
    public void markEventUsingProgress() throws Exception {
        int event_id = 1;

        mockMvc.perform(put("/events/" + event_id + "/progress")
                .content("{\"goal\":10000}")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.events.*", hasSize(1)))
                .andExpect(jsonPath("$.events[0].id", is(event_id)))
                .andExpect(jsonPath("$.events[0].goal", is(10000)));
    }

    @Test
    public void consumeNonCompletedEvent() throws Exception {
        int event_id = 1;
        mockMvc.perform(post("/events/" + event_id + "/collect")
                .contentType(contentType))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void consumeCompletedEvent() throws Exception {
        int event_id = 2;
        mockMvc.perform(post("/events/" + event_id + "/collect")
                .contentType(contentType))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.events.*", hasSize(1)));
    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }
}
