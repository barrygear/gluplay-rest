You will need java 1.8 installed.

To build/run:

$ ./mvnw clean compile package spring-boot:run

For a first time user of maven, it *will* download lots of files. These can be removed later using 
`
rm -rf ~/.m2
`
# Comments

Things that can be done to this code

1. Add Swagger for live REST documentation
1. Add caching for events
1. Add Metrics for RPS graphings
1. Add single file executable target for micro-service deployment
1. More unit tests

* * *

# REST Service Definition

* GET /events
* PARAM device_id 
* TYPE int
* RETURN List of events
* DESCRIPTION Returns the events stored for the device_id. If no events found for device_id, returns an empty list. 

EXAMPLES

List events
```
$ curl -s http://localhost:8080/events?device_id=1 | jq
{
  "events": [
    {
      "id": 1,
      "event_type": "collect_flo",
      "goal": 100,
      "start": "2016-11-21T17:23:39.632Z",
      "end": "2016-11-21T17:23:39.632Z",
      "status": "completed",
      "reward": {
        "type": "soft_currency",
        "value": 10000
      }
    },
    {
      "id": 2,
      "event_type": "cookie_madness",
      "goal": 125,
      "start": "2016-11-21T17:23:39.632Z",
      "end": "2016-11-21T17:23:39.632Z",
      "status": "collection",
      "reward": {
        "type": "soft_currency",
        "value": 5000
      }
    },
    {
      "id": 3,
      "event_type": "fast_flo",
      "goal": 80,
      "start": "2016-11-21T17:23:39.632Z",
      "end": "2016-11-21T17:23:39.632Z",
      "status": "active",
      "reward": {
        "type": "hard_currency",
        "value": 10
      }
    }
  ]
}
```

List events with invalide device_id (return 404? or 5xx?)
```
$ curl -s http://localhost:8080/events?device_id=0 | jq
{
  "events": []
}
```
* * *

* PUT /events/{event_id}/progress
* PATHPARAM event_id 
    * TYPE int
* PUT PAYLOAD A map of key/values to change. Keys must match names of event fields.
* RETURN updated event
* DESCRIPTION Updates fields of an event with values. All non null values are applied to the data and saved. 

EXAMPLES

Change value with invalid parameter
```
$ curl -s -i -X PUT -H "Content-Type: application/json" -d "{\"event_type\":\"cookie_madness\",\"goal\": 500,\"start\": \"2016-11-21T17:23:39.632Z\",\"end\": \"2016-11-21T17:23:39.632Z\",\"status\": \"collection\",\"reward\": {\"type\": \"soft_currency\",\"value\": 5000}, \"id\":1}"  http://localhost:8080/events/2/progress
HTTP/1.1 400
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Mon, 21 Nov 2016 19:14:23 GMT
Connection: close

{"timestamp":1479755663868,"status":400,"error":"Bad Request","exception":"net.junkyboy.rest.InvalidRequestException","message":"Cannot change id or device_id","path":"/events/2/progress"}
```

Change goal to 500
```
$ curl -s -X PUT -H "Content-Type: application/json" -d "{\"event_type\": \"cookie_madness\",\"goal\": 500,\"start\": \"2016-11-21T17:23:39.632Z\",\"end\": \"2016-11-21T17:23:39.632Z\",\"status\": \"collection\",\"reward\": {\"type\": \"soft_currency\",\"value\": 5000}}"  http://localhost:8080/events/2/progress | jq
{
  "events": [
    {
      "id": 2,
      "event_type": "cookie_madness",
      "goal": 500,
      "start": "2016-11-21T17:23:39.632Z",
      "end": "2016-11-21T17:23:39.632Z",
      "status": "collection",
      "reward": {
        "type": "soft_currency",
        "value": 5000
      }
    }
  ]
}
HTTP/1.1 200
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Mon, 21 Nov 2016 17:52:58 GMT
```
List events after change
```
$ curl -s http://localhost:8080/events?device_id=1 | jq
{
  "events": [
    {
      "id": 1,
      "event_type": "collect_flo",
      "goal": 100,
      "start": "2016-11-21T18:27:51.333Z",
      "end": "2016-11-21T18:27:51.333Z",
      "status": "completed",
      "reward": {
        "type": "soft_currency",
        "value": 10000
      }
    },
    {
      "id": 2,
      "event_type": "cookie_madness",
      "goal": 500,
      "start": "2016-11-21T17:23:39.632Z",
      "end": "2016-11-21T17:23:39.632Z",
      "status": "collection",
      "reward": {
        "type": "soft_currency",
        "value": 5000
      }
    },
    {
      "id": 3,
      "event_type": "fast_flo",
      "goal": 80,
      "start": "2016-11-21T18:27:51.334Z",
      "end": "2016-11-21T18:27:51.334Z",
      "status": "active",
      "reward": {
        "type": "hard_currency",
        "value": 10
      }
    }
  ]
}
```
* * *

* POST /events/{event_id}/collect
* PATHPARAM event_id
    * TYPE int
* RETURN consumed collectable event
* DESCRIPTION This changes events from collectable to collected state. Adds reward to client 
EXAMPLES

Invalid request
```
$ curl -s -i -X PUT -H "Content-Type: application/json"  http://localhost:8080/events/2/collection
HTTP/1.1 404
Content-Length: 0
Date: Mon, 21 Nov 2016 19:17:00 GMT
```

Collect event 2
```
$ curl -s -i -X POST -H "Content-Type: application/json"  http://localhost:8080/events/2/collect
HTTP/1.1 200
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Mon, 21 Nov 2016 19:17:38 GMT

{"events":[{"id":2,"event_type":"cookie_madness","goal":500,"start":"2016-11-21T17:23:39.632Z","end":"2016-11-21T17:23:39.632Z","status":"collected","reward":{"type":"soft_currency","value":5000}}]}
```

Invalid collection
```
$ curl -s -i -X POST -H "Content-Type: application/json"  http://localhost:8080/events/1/collect
HTTP/1.1 400
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Mon, 21 Nov 2016 19:17:58 GMT
Connection: close

{"timestamp":1479755878053,"status":400,"error":"Bad Request","exception":"net.junkyboy.rest.InvalidRequestException","message":"event_id 1 not available for collections","path":"/events/1/collect"}
```
Show status for event 2
```
$ curl -s http://localhost:8080/events?device_id=1 | jq
{
  "events": [
    {
      "id": 1,
      "event_type": "collect_flo",
      "goal": 100,
      "start": "2016-11-21T18:27:51.333Z",
      "end": "2016-11-21T18:27:51.333Z",
      "status": "completed",
      "reward": {
        "type": "soft_currency",
        "value": 10000
      }
    },
    {
      "id": 2,
      "event_type": "cookie_madness",
      "goal": 500,
      "start": "2016-11-21T17:23:39.632Z",
      "end": "2016-11-21T17:23:39.632Z",
      "status": "collected",
      "reward": {
        "type": "soft_currency",
        "value": 5000
      }
    },
    {
      "id": 3,
      "event_type": "fast_flo",
      "goal": 80,
      "start": "2016-11-21T18:27:51.334Z",
      "end": "2016-11-21T18:27:51.334Z",
      "status": "active",
      "reward": {
        "type": "hard_currency",
        "value": 10
      }
    }
  ]
}
```